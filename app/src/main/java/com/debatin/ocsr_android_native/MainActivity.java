package com.debatin.ocsr_android_native;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {
    private Timer mTimer1;
    private TimerTask mTt1;
    private Handler mTimerHandler = new Handler();
    private int memoryMax = 0;
    private int memoryMin = 0;
    private int memorySum = 0;
    private int memoryCount = 0;
    private int cpuMax = 0;
    private int cpuMin = 0;
    private int cpuSum = 0;
    private int cpuCount = 0;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        File file = new File (getFilesDir(), "configuration");
        if (!file.exists()) { //Primeira vez
            AsyncTaskInit runnerInit = new AsyncTaskInit();
            runnerInit.execute();
        } else {
            AsyncTaskTestar runnerTeste = new AsyncTaskTestar();
            runnerTeste.execute();
        }
    }

    private String retornaData() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
        Date data = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        Date data_atual = cal.getTime();
        return dateFormat.format(data_atual);
    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        try {
            String[] assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = getFilesDir().toString()+ "/" + path;
                File dir = new File(fullPath);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                for (String asset : assets) {
                    //Log.d("Debatin", path + "/" + asset);
                    copyFileOrDir(path + "/" + asset);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = this.getAssets();
        InputStream in;
        OutputStream out;
        try {
            in = assetManager.open(filename);
            String newFileName = getFilesDir().toString()+ "/" + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    private void stopTimer(){
        if(mTimer1 != null){
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    public ArrayList<String> getCPUUsage() {
        ArrayList<String> result = new ArrayList<>();
        try {
            if (Build.VERSION.SDK_INT < 26) {
                String[] cmd = {
                        "sh",
                        "-c",
                        "top -n 1"};
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    //Log.d("Debatin", "linha: " + line);
                    result.add(line);
                }
            } else { //Android 8.0, 8.1 e 9.0
                String[] cmd = {
                        "sh",
                        "-c",
                        "ps -eo pcpu,pid,user,args"};
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    //Log.d("Debatin", "linha: " + line);
                    result.add(line);
                }
            }
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
        return result;
    }

    private void startTimer(final int corpusConf){
        long TEMPO = (1000 * 25); // chama o método a cada 25 segundos
        if (corpusConf == 1) {
            TEMPO = (1000 * 5); // chama o método a cada 5 segundos
        }
        mTimer1 = new Timer();
        mTt1 = new TimerTask() {
            public void run() {
                mTimerHandler.post(new Runnable() {
                    public void run(){
                        String corpus = "CBR";
                        if (corpusConf == 1) {
                            corpus = "LAPS";
                        }
                        //Log.d("Debatin", "Corpus: " + corpus);
                        //CPU
                        int position;
                        int percentCPU = 0;
                        if (Build.VERSION.SDK_INT < 24) { //6.0 inferior
                            try {
                                String dirFile = getFilesDir().toString()+"/testPerformance" + corpus + ".txt";
                                File file = new File(dirFile);
                                file.delete();
                                stringFromJNI(corpusConf, 2, getFilesDir().toString());
                                if (file.exists()) {
                                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                                    String line;
                                    String retornoAux = "";
                                    while ((line = reader.readLine()) != null) {
                                        retornoAux += line + "\n";
                                        break;
                                    }
                                    position = retornoAux.indexOf("%");
                                    percentCPU = Integer.parseInt(retornoAux.substring(position - 2, position).trim());
                                    //Log.d("Debatin", "Percentual: " + percentCPU);
                                    if (percentCPU > cpuMax) {
                                        cpuMax = percentCPU;
                                    }
                                    if (cpuMin == 0) {
                                        cpuMin = percentCPU;
                                    }
                                    if (percentCPU < cpuMin && percentCPU != 0) {
                                        cpuMin = percentCPU;
                                    }
                                    cpuSum += percentCPU;
                                    cpuCount += 1;
                                }
                            } catch (Exception e) {
                                Log.e("tag", e.getMessage());
                            }
                        } else { //7.0 superior
                            ArrayList<String> retorno = getCPUUsage();
                            if (!retorno.isEmpty()) {
                                if (Build.VERSION.SDK_INT < 26) { //7.0 e 7.1
                                    for (int i = 0; i < retorno.size(); i++) {
                                        if (retorno.get(i).indexOf("System") > 0) {
                                            //Log.d("Debatin", "retorno: " + retorno.get(i));
                                            String[] array = retorno.get(i).split(",");
                                            for (int j = 0; j < array.length; j++) {
                                                //Log.d("Debatin", "retorno: " + array[j]);
                                                position = array[j].indexOf("%");
                                                percentCPU += Integer.parseInt(array[j].substring(position - 2, position).trim());
                                            }
                                            break;
                                        }
                                    }
                                    if (percentCPU > cpuMax) {
                                        cpuMax = percentCPU;
                                    }
                                    if (cpuMin == 0) {
                                        cpuMin = percentCPU;
                                    }
                                    if (percentCPU < cpuMin && percentCPU != 0) {
                                        cpuMin = percentCPU;
                                    }
                                    cpuSum += percentCPU;
                                    cpuCount += 1;
                                } else { //Android 8.0, 8.1 e 9.0
                                    for (int i = 1; i < retorno.size(); i++) {
                                        //Log.d("Debatin", "retorno.get(i): " + retorno.get(i));
                                        percentCPU += (int) Float.parseFloat(retorno.get(i).substring(0, 4).trim());
                                    }
                                    //Log.d("Debatin", "CPU: " + percentCPU);
                                    if (percentCPU > cpuMax) {
                                        cpuMax = percentCPU;
                                    }
                                    if (cpuMin == 0) {
                                        cpuMin = percentCPU;
                                    }
                                    if (percentCPU < cpuMin && percentCPU != 0) {
                                        cpuMin = percentCPU;
                                    }
                                    cpuSum += percentCPU;
                                    cpuCount += 1;
                                }
                            }
                        }
                        //Memória
                        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
                        Map<Integer, String> pidMap = new TreeMap<>();
                        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                            if (runningAppProcessInfo.processName.equals(BuildConfig.APPLICATION_ID)) {
                                pidMap.put(runningAppProcessInfo.pid, runningAppProcessInfo.processName);
                            }
                        }
                        Collection<Integer> keys = pidMap.keySet();
                        for(int key : keys) {
                            int pids[] = new int[1];
                            pids[0] = key;
                            android.os.Debug.MemoryInfo[] memoryInfoArray = activityManager.getProcessMemoryInfo(pids);
                            for(android.os.Debug.MemoryInfo pidMemoryInfo : memoryInfoArray) {
                                int getTotalPss = pidMemoryInfo.getTotalPss();
                                //Log.d("Debatin", "Memoria: " + getTotalPss);
                                if (getTotalPss > memoryMax) {
                                    memoryMax = getTotalPss;
                                }
                                if (memoryMin == 0) {
                                    memoryMin = getTotalPss;
                                }
                                if (getTotalPss < memoryMin && getTotalPss != 0) {
                                    memoryMin = getTotalPss;
                                }
                                memorySum += getTotalPss;
                                memoryCount += 1;
                            }
                        }
                    }
                });
            }
        };
        mTimer1.schedule(mTt1, TEMPO, TEMPO);
    }

    private float batteryLevel () {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, iFilter);
        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;
        float batteryPct = level / (float) scale;
        return (batteryPct * 100);
    }

    private void testar() {
        String mensagem = "Biblioteca: CMUSphinx\n";
        mensagem += "Marca: " + Build.MANUFACTURER + " - Modelo: " + Build.MODEL + "\n";
        mensagem += "Versão do Android: " + Build.VERSION.RELEASE + "\n\n";
        for (int corpusConf = 0; corpusConf < 2; corpusConf++) {
            String corpus = "CBR";
            if (corpusConf == 1) {
                corpus = "LAPS";
            }
            memoryMax = 0;
            memoryMin = 0;
            memorySum = 0;
            memoryCount = 0;
            cpuMax = 0;
            cpuMin = 0;
            cpuSum = 0;
            cpuCount = 0;
            mensagem += "Corpus: " + corpus + "\n";
            //Processamento inicial
            int percentCPUSum = 0;
            if (Build.VERSION.SDK_INT < 24) { //6.0 inferior
                try {
                    String dirFile = getFilesDir().toString() + "/testPerformanceIni.txt";
                    File file = new File(dirFile);
                    file.delete();
                    stringFromJNI(corpusConf, 1, getFilesDir().toString());
                    if (file.exists()) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                        String line;
                        String retornoAux = "";
                        while ((line = reader.readLine()) != null) {
                            retornoAux += line + "\n";
                            break;
                        }
                        int position;
                        String array[] = retornoAux.split(",");
                        for (int i = 0; i < array.length; i++) {
                            position = array[i].indexOf("%");
                            percentCPUSum += Integer.parseInt(array[i].substring(position - 2, position).trim());
                        }
                    }
                } catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }
            } else { //7.0 superior
                ArrayList<String> retorno = getCPUUsage();
                if (!retorno.isEmpty()) {
                    if (Build.VERSION.SDK_INT < 26) { //7.0 e 7.1
                        for (int i = 0; i < retorno.size(); i++) {
                            if (retorno.get(i).indexOf("System") > 0) {
                                //Log.d("Debatin", "retorno: " + retorno.get(i));
                                String[] array = retorno.get(i).split(",");
                                for (int j = 0; j < array.length; j++) {
                                    //Log.d("Debatin", "retorno: " + array[j]);
                                    int position = array[j].indexOf("%");
                                    percentCPUSum += Integer.parseInt(array[j].substring(position - 2, position).trim());
                                }
                                break;
                            }
                        }
                    } else { //Android 8.0, 8.1 e 9.0
                        for (int i = 1; i < retorno.size(); i++) {
                            percentCPUSum += (int) Float.parseFloat(retorno.get(i).substring(0, 4).trim());
                        }
                    }
                }
            }
            mensagem += "Percentual de processamento inicial: " + percentCPUSum + "\n";
            //Log.d("Debatin", "percentCPUSum: " + percentCPUSum);
            //Memória Inicial
            ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            mensagem += "Memória disponível: " + memoryInfo.availMem + " - Memória baixa: " + memoryInfo.lowMemory + " - Memória total: " + memoryInfo.totalMem + "\n";
            startTimer(corpusConf);
            mensagem += "Percentual de bateria início: " + batteryLevel() + "%\n";
            mensagem += "Data início: " + retornaData() + "\n";
            stringFromJNI(corpusConf, 0, getFilesDir().toString());
            mensagem += "Data final: " + retornaData() + "\n";
            mensagem += "Percentual de bateria fim: " + batteryLevel() + "%\n";
            stopTimer();
            int memoryAVG = 0;
            if (memoryCount > 0) {
                memoryAVG = memorySum / memoryCount;
            }
            mensagem += "Memória máx: " + memoryMax + " - Memória min: " + memoryMin + " - Memória méd: " +  memoryAVG + "\n";
            int cpuAVG = 0;
            if (cpuCount > 0) {
                cpuAVG = cpuSum / cpuCount;
            }
            mensagem += "Processamento máx: " + cpuMax + " - Processamento min: " + cpuMin + " - Processamento méd: " + cpuAVG + "\n\n";
        }
        //Log.d("Debatin", "Mensagem: " + mensagem);
        Intent enviar = new Intent(Intent.ACTION_SEND);
        enviar.putExtra(Intent.EXTRA_SUBJECT, "Arquivo de testes OCSR");
        enviar.setType("text/txt");
        enviar.putExtra(Intent.EXTRA_TEXT, mensagem);
        startActivity(Intent.createChooser(enviar, "Enviar mensagem de testes para: "));
    }

    private class AsyncTaskTestar extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        //https://stackoverflow.com/questions/2298208/how-do-i-discover-memory-usage-of-my-application-in-android
        @Override
        protected String doInBackground(String... params) {
            testar();
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
        }
        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Aguarde","Gerando o arquivo de testes...");
        }
    }

    private class AsyncTaskInit extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        @Override
        protected String doInBackground(String... params) {
            File file = new File (getFilesDir(), "configuration");
            String content =  "0-0";
            FileOutputStream outputStream;
            try {
                outputStream = new FileOutputStream(file);
                outputStream.write(content.getBytes());
                outputStream.close();
            } catch (IOException e) {
                Log.e("tag", e.getMessage());
            }
            String[] diretorios = {"model", "tests"};
            for (String dir : diretorios) {
                copyFileOrDir(dir);
            }
            //LAPS
            File dir = new File(getFilesDir().toString()+"/tests/laps/corpus");
            File[] arquivos = dir.listFiles();
            String nomeArquivosLaps = "";
            for (File a : arquivos) {
                if (a.getPath().contains("wav")) {
                    nomeArquivosLaps += a.getName() + "\n";
                }
            }
            File arquivoLaps = new File(getFilesDir(), "nomeArquivosLaps" );
            try {
                outputStream = new FileOutputStream(arquivoLaps);
                outputStream.write(nomeArquivosLaps.getBytes());
                outputStream.close();
            } catch (IOException e) {
                Log.e("tag", e.getMessage());
            }

            //CBR
            dir = new File(getFilesDir().toString()+"/tests/cbr/corpus");
            arquivos = dir.listFiles();
            String nomeArquivosCBR = "";
            for (File a : arquivos) {
                if (a.getPath().contains("wav")) {
                    nomeArquivosCBR += a.getName() + "\n";
                }
            }
            File arquivoCBR = new File(getFilesDir(), "nomeArquivosCBR" );
            try {
                outputStream = new FileOutputStream(arquivoCBR);
                outputStream.write(nomeArquivosCBR.getBytes());
                outputStream.close();
            } catch (IOException e) {
                Log.e("tag", e.getMessage());
            }
            //Testar
            testar();
            return "";
        }
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
        }
        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Aguarde","Inicializando o aplicativo e gerando o arquivo de testes...");
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI(int parm1, int parm2, String parm3);
}
