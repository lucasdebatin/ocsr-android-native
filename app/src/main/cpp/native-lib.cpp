#include <jni.h>
#include <string>
#include "cmusphinx.h"
#include <fstream>
#include <iostream>
#include <android/log.h>

extern "C" JNIEXPORT jstring JNICALL
Java_com_debatin_ocsr_1android_1native_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */,
        jint corpus,
        jint cpuUsage,
        jstring caminho) {

    //converte jstring em std::string
    const jclass stringClass = env->GetObjectClass(caminho);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(caminho, getBytes, env->NewStringUTF("UTF-8"));
    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);
    std::string retCaminho = std::string((char *)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);
    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    //converte jstring em std::string

    std::string retorno = "";
    if (cpuUsage == 1) {
        std::string command = "top -n 1 | grep \"System\" > " + retCaminho + "/testPerformanceIni.txt";
        //__android_log_print(ANDROID_LOG_VERBOSE, "Debatin", command.c_str(), 1);
        system(command.c_str());
    } else if (cpuUsage == 2) {
        std::string corpusNome = "CBR";
        if (corpus == 1) {
            corpusNome = "LAPS";
        }
        std::string command = "top -n 1 | grep \"com.debatin.ocsr_android_native\" > " + retCaminho + "/testPerformance" + corpusNome + ".txt";
        //__android_log_print(ANDROID_LOG_VERBOSE, "Debatin", command.c_str(), 1);
        system(command.c_str());
    } else {
        CMUSphinx cmu;
        retorno = cmu.test(corpus, retCaminho);
    }

    return env->NewStringUTF(retorno.c_str());

}
