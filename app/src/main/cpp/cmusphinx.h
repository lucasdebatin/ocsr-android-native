#ifndef CMUSPHINX_H
#define CMUSPHINX_H
#include <pocketsphinx.h>
#include <string>

class CMUSphinx
{
public:
    CMUSphinx();
    std::string test(int, std::string);
};

#endif // CMUSPHINX_H
