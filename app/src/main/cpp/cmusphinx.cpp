#include "cmusphinx.h"
#include <iostream>
#include <fstream>

CMUSphinx::CMUSphinx() {}

std::string CMUSphinx::test(int corpus, std::string caminho) {
    ps_decoder_t *ps;
    cmd_ln_t *config;
    FILE *fh;
    char const *hyp;
    int16 buf[512];
    int rv;
    int32 score;

    std::string corpusNome = "cbr";
    if (corpus == 1) {
        corpusNome = "laps";
    }
    std::string model = caminho + "/model/cmusphinx/" + corpusNome + "/model_parameters";
    std::string lm = caminho + "/model/cmusphinx/" + corpusNome + "/ocsr.lm.ARPA";
    std::string dic = caminho + "/model/cmusphinx/" + corpusNome + "/ocsr.dic";

    config = cmd_ln_init(nullptr, ps_args(), TRUE,
                         "-hmm", model.c_str(),
                         "-lm", lm.c_str(),
                         "-dict", dic.c_str(),
                         NULL);


    if (config == nullptr) {
        fprintf(stderr, "Failed to create config object, see log for details\n");
        return "";
    }

    ps = ps_init(config);
    if (ps == nullptr) {
        fprintf(stderr, "Failed to create recognizer, see log for details\n");
        return "";
    }
    std::string result = "";
    std::string arquivo;
    if (corpus == 0) { //CBR
        arquivo = caminho + "/nomeArquivosCBR";
    } else { //Laps
        arquivo = caminho + "/nomeArquivosLaps";
    }
    std::ifstream inFile (arquivo.c_str());
    std::string linha;
    if (inFile.is_open()) {
        while (getline(inFile, linha)) {
            std::string arquivoPath;
            if (corpus == 0) { //CBR
                arquivoPath = caminho + "/tests/cbr/corpus/" + linha;
            } else { //Laps
                arquivoPath = caminho + "/tests/laps/corpus/" + linha;
            }
            fh = fopen(arquivoPath.c_str(), "rb");
            if (fh == nullptr) {
                fprintf(stderr, "Unable to open input file\n");
                return "";
            }
            rv = ps_start_utt(ps);
            while (!feof(fh)) {
                size_t nsamp;
                nsamp = fread(buf, 2, 512, fh);
                rv = ps_process_raw(ps, buf, nsamp, FALSE, FALSE);
            }
            rv = ps_end_utt(ps);
            hyp = ps_get_hyp(ps, &score);
            result += linha;
            result += " - ";
            result += hyp;
            result += "\n";
            fclose(fh);
        }
        inFile.close();
    }
    ps_free(ps);
    cmd_ln_free_r(config);

    return result;
}

